#!/usr/bin/env python
import os
import slumber
from optparse import OptionParser

from firebase import firebase
fb = firebase.FirebaseApplication('https://vfx.firebaseio.com', None)
authentication = firebase.FirebaseAuthentication('SECRET', 'EMAIL@gmail.com')
fb.authentication = authentication


parser = OptionParser()
parser.add_option('--project', dest = 'project', default = None, help = 'project')
(options, args) = parser.parse_args()



shots = fb.get('/project/' + options.project + '/shots', None)

for sid, shot in shots.iteritems() :

	scene = shot['sequence'].upper()
	shot_name = ('%s_%s') % (scene, shot['code'])
	print shot_name


	pwd = os.getcwd()
	paths = []
	dirs = ['dailies',
			 'hires',
			 'animation',
			 'tracking/proxy',
			 'scripts/nk',
			 'images/img',
			 'images/render',
			 'images/refs',
			 'images/tex',
			 '3d/houdini',
			 '3d/maya']
	for i in dirs:
		paths.append(os.path.join(pwd, options.project.upper(), 'comp', scene, shot['code'], i))
		paths.append(os.path.join(pwd, options.project.upper(), 'footages', scene, shot['code']))

	paths.append(os.path.join(pwd, options.project.upper(), 'finals'))
		
	for item in paths:
		if not os.path.exists(item):
			os.makedirs(item)

