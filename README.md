# README #

Postproduction tools

### What is this repository for? ###

* Examples of using shottyapp API
* Version 0.0.1 beta

### How do I get set up? ###

* Dependencies

```
#!bash

$ sudo pip install requests
$ sudo pip install python-firebase
```

* How to run 

```
#!bash

shotty-create-project-dirs.py --project=CODE
```


### Who do I talk to? ###

* Repo owner